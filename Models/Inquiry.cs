using System;
using System.Collections.Generic;

namespace RedisAndDynamoDb.Models
{
    public class Inquiry
    {
        public Guid InquiryId {get;set;}
        public DateTime Created {get;set;}
        public string FirstName {get;set;}
    }

}