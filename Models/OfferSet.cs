using System;

namespace RedisAndDynamoDb.Models
{
    public class OfferSet
    {
        public Guid OfferSetId {get;set;}
        public DateTime Created {get;set;}
        public string FirstName {get;set;}
    }
}

