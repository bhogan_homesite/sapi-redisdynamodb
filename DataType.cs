namespace RedisAndDynamoDb
{
    public enum DataType
    {
        Offer = 0,
        OfferSet = 1,
        Question = 2,
        Answer = 3,
        Inquiry = 4
    }
}