using System;

namespace RedisAndDynamoDb
{
    public class WriteResponse
    {
        public string Message {get;set;}
        public Status Status {get;set;}
    }
    public enum Status 
    {
        Success = 0,
        Failure = 1
    }
}
