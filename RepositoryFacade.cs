using System;
using System.Collections.Generic;
using System.Linq;
using RedisAndDynamoDb.DbRepositories;
using System.Text.Json;

namespace RedisAndDynamoDb
{
    public class RepositoryFacade<K1, K2, T> : IRepositoryFacade<K1, K2, T> 
    {
        List<Repository> dbRepositories;

        public RepositoryFacade(DbChoice dbChoice) // we should get this from configuration code
        {
            dbRepositories = new RepositoryFactory().Create(dbChoice).ToList();
        }

            public T Read(K1 key1, K2 key2)
            {
                Dictionary<string, T> responses = new Dictionary<string, T>();

                foreach (var repo in dbRepositories)
                {
                    T data = JsonSerializer.Deserialize<T>(repo.Read(key1.ToString(), key2.ToString(), GetDataType()), new JsonSerializerOptions() {PropertyNameCaseInsensitive = true});
                    responses.Add(repo.GetType().Name,  data);
                }
                responses.ToList().ForEach(r => Console.WriteLine($"{r.Key} {r.Value}"));
                return responses.First().Value;
            }

        public Status Write(K1 key1, K2 key2, T dataToWrite)
        {
            Dictionary<string, Status> responses = new Dictionary<string, Status>();
            foreach (var repo in dbRepositories)
            {
                string jsonToWrite = JsonSerializer.Serialize(dataToWrite);
                responses.Add(repo.GetType().Name, repo.Write(jsonToWrite, key1.ToString(), key2.ToString(), GetDataType()));
            }
            responses.ToList().ForEach(r => Console.WriteLine($"{r.Key} {r.Value}"));
            return responses.First().Value;
        }


        private DataType GetDataType()
        {
            string type = typeof(T).ToString().Split(".").Last();
            Enum.TryParse(type, out DataType dataType);
            return dataType;
        }
    }
}
