﻿using System;
using RedisAndDynamoDb.DbRepositories;
using RedisAndDynamoDb.Models;

namespace RedisAndDynamoDb
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting");
            OfferSet offerSetToWrite = new OfferSet() {OfferSetId = Guid.NewGuid(), Created = DateTime.Now, FirstName = "Dennie"};

            DbChoice dbChoice = DbChoice.DynamoDbAndRedis; // get this from config code
            RepositoryFacade<string, string, OfferSet> offerSetRepository = new RepositoryFacade<string, string, OfferSet>(dbChoice);
            Status offerSetWriteStatus = offerSetRepository.Write("myOfferSetKey1", "myOfferSetKey2", offerSetToWrite);
            
            OfferSet offerSetReturned = offerSetRepository.Read("someOfferSetKey1", "someOfferSetKey2");


            Inquiry inquiryToWrite  = new Inquiry() {InquiryId = Guid.NewGuid(), Created = DateTime.Now, FirstName = "Dennie"};

            RepositoryFacade<string, string, Inquiry> inquiryRepository = new RepositoryFacade<string, string, Inquiry>(dbChoice);
            Status inquiryWriteStatus = inquiryRepository.Write("myInquiryKey1", "myInquiryKey2", inquiryToWrite);
            Inquiry inquiryReturned = inquiryRepository.Read("someInquiryKey1", "someInquiryKey2");
        }
    }
}
