using System;

namespace RedisAndDynamoDb.DbRepositories
{
    public class RedisRepository : Repository
    {
        public override Status Write(string jsonData, string key1, string key2, DataType dataType)
        {
            Console.WriteLine($"Wrote to Redis with data:{jsonData}\n with key1:{key1} and key2:{key2}");
            return Status.Success;
        }

        public override string Read(string key1, string key2, DataType dataType)
        {
            if (dataType == DataType.OfferSet)
            {
                return "{ \"offerSetId\": \"b43eb16a-f4b1-40b2-823f-c7e598b16200\", \"created\": \"2020-06-28T00:15:00\", \"firstName\":\"Redis Dave\" }";
            }
            else
            {
                return "{ \"inquiryId\": \"3d97a452-6dd8-4b28-bd83-9f1ea3b420ca\", \"created\": \"2020-06-20T00:11:10\", \"firstName\":\"Redis Dave\" }";
            }
        }
    }
}
