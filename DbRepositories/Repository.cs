using System;

namespace RedisAndDynamoDb.DbRepositories
{
    public abstract class Repository
    {
        // key1 and key2 might need to be generic. If we go with Repository<K1, K2, T> then we don't need dataType.
        public abstract Status Write(string jsonData, string key1, string key2, DataType dataType);
        public abstract string Read(string key1, string key2, DataType dataType);
    }
}
