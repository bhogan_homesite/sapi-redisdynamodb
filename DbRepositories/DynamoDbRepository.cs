using System;

namespace RedisAndDynamoDb.DbRepositories
{
    public class DynamoDbRepository : Repository
    {
        public override Status Write(string jsonData, string key1, string key2, DataType dataType)
        {
            string tableName = GetTableName(dataType);
            Console.WriteLine($"Wrote to DynamoDb with data:{jsonData}\n with key1:{key1} and key2:{key2}");
            return Status.Success;
        }

        public override string Read(string key1, string key2, DataType dataType)
        {
            // based on the the data type we know what table to read from
            if (dataType == DataType.OfferSet)
            {
                return "{ \"offerSetId\": \"b43eb16a-f4b1-40b2-823f-c7e598b16200\", \"created\": \"2020-06-28T00:15:00\", \"firstName\":\"Dynamo Dave\" }";
            }
            else
            {
                return "{ \"inquiryId\": \"3d97a452-6dd8-4b28-bd83-9f1ea3b420ca\", \"created\": \"2020-06-20T00:11:10\", \"firstName\":\"Dynamo Dave\" }";
            }
        }
        private string GetId(string data, DataType dataType)
        {
            // get the id from the data
            return Guid.NewGuid().ToString();
        }
        private string GetTableName(DataType dataType)
        {
            // get this from config, or maybe we stick to a convention that the enum name is the table name?
            switch (dataType)
            {
                case DataType.Inquiry:
                    return "InquiryTableName"; 
                case DataType.OfferSet:
                    return "OfferSetTableName"; 
                default:
                    throw new NotImplementedException("Some DataType I have not heard of");
            }
        }
    }
}
