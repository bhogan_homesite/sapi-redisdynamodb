using System;
using System.Collections.Generic;

namespace RedisAndDynamoDb.DbRepositories
{
    public class RepositoryFactory
    {
        public IList<Repository> Create(DbChoice dbChoice)
        {
            IList<Repository> dbRepositories = new List<Repository>();
            switch (dbChoice)
            {
                case DbChoice.DynamoDb:
                    dbRepositories.Add(new DynamoDbRepository());
                    break;
                case DbChoice.Redis:
                    dbRepositories.Add(new RedisRepository());
                    break;
                case DbChoice.DynamoDbAndRedis:
                    dbRepositories.Add(new DynamoDbRepository());
                    dbRepositories.Add(new RedisRepository());
                    break;
                default:
                    throw new NotImplementedException("Some choice of db that is not supported");
            }
            return dbRepositories;
        }
    }

    public enum DbChoice{
        DynamoDb = 0,
        Redis = 1,
        DynamoDbAndRedis = 2
    }
}
