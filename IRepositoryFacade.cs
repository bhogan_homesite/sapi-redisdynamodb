namespace RedisAndDynamoDb
{
    public interface IRepositoryFacade<K1, K2, T>
    {
        Status Write(K1 key1, K2 key2, T dataToWrite);
        T Read(K1 key1, K2 key2);
    }
}


